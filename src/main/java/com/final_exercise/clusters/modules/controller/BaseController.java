package com.final_exercise.clusters.modules.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

interface BaseController<T> {
    ResponseEntity<T> create(@RequestBody T t);

    ResponseEntity<T> getById(int id);

    ResponseEntity<List<T>> listAll();

    @ExceptionHandler(IllegalArgumentException.class)
    ResponseEntity<String> handleIllegalArgumentException(Exception e);
}
