package com.final_exercise.clusters.modules.controller;

import com.final_exercise.clusters.modules.model.cluster.Cluster;
import com.final_exercise.clusters.modules.model.user.User;
import com.final_exercise.clusters.modules.persistence.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController implements BaseController<User> {

    @Autowired
    private UserService service;

    @Override
    @PostMapping("/user")
    public ResponseEntity<User> create(@RequestBody User user) {
        if (user.getName() == null)
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        return new ResponseEntity<>(service.create(user), HttpStatus.CREATED);
    }

    @PutMapping("/user/{id}/deactivate")
    public ResponseEntity<User> deactivate(@PathVariable("id") int id) {
        User user = service.getById(id);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            user.setActive(false);
            service.update(user);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }

    }

    @Override
    @GetMapping("/user/{id}")
    public ResponseEntity<User> getById(@PathVariable("id") int id) {
        User user = service.getById(id);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
    }

    @GetMapping("/user/by_name/{name}")
    public ResponseEntity<User> getByName(@PathVariable("name") String name) {
        User user = service.getByName(name);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
    }

    @Override
    @GetMapping("/users")
    public ResponseEntity<List<User>> listAll() {
        return new ResponseEntity<>(service.getAll(), HttpStatus.OK);
    }

    @GetMapping("/user/{id}/clusters")
    public ResponseEntity<List<Cluster>> listAssignedClusters(@PathVariable int id) {
        User user = service.getById(id);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            List<Cluster> clusters = new ArrayList<>(user.getClusters());
            return new ResponseEntity<>(clusters, HttpStatus.OK);
        }
    }

    @Override
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleIllegalArgumentException(Exception e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
