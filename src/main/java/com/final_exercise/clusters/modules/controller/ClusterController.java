package com.final_exercise.clusters.modules.controller;

import com.final_exercise.clusters.modules.model.cluster.Cluster;
import com.final_exercise.clusters.modules.model.server.Server;
import com.final_exercise.clusters.modules.model.user.User;
import com.final_exercise.clusters.modules.persistence.service.cluster.ClusterService;
import com.final_exercise.clusters.modules.persistence.service.server.ServerService;
import com.final_exercise.clusters.modules.persistence.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ClusterController implements BaseController<Cluster> {

    @Autowired
    private ClusterService clusterService;

    @Autowired
    private UserService userService;

    @Autowired
    private ServerService serverService;

    @Override
    @PostMapping("/cluster")
    public ResponseEntity<Cluster> create(@RequestBody Cluster cluster) {
        if (cluster.getName() == null)
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        return new ResponseEntity<>(clusterService.create(cluster), HttpStatus.CREATED);
    }

    @Override
    @GetMapping("/cluster/{id}")
    public ResponseEntity<Cluster> getById(@PathVariable int id) {
        Cluster cluster = clusterService.getById(id);
        if (cluster == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(cluster, HttpStatus.OK);
        }
    }

    @Override
    @GetMapping("/clusters")
    public ResponseEntity<List<Cluster>> listAll() {
        return new ResponseEntity<>(clusterService.getAll(), HttpStatus.OK);
    }

    @Override
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleIllegalArgumentException(Exception e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/cluster/{c_id}/assign_user/{u_id}")
    public ResponseEntity<Cluster> assign(@PathVariable("c_id") int clusterId, @PathVariable("u_id") int userId) {
        Cluster cluster = clusterService.getById(clusterId);
        if (cluster == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        User user = userService.getById(userId);
        if (user == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        cluster.getUsers().add(user);
        clusterService.update(cluster);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);

    }

    @GetMapping("/cluster/{id}/servers")
    public ResponseEntity<List<Server>> listServers(@PathVariable int id) {
        Cluster cluster = clusterService.getById(id);
        if (cluster == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            List<Server> servers = new ArrayList<>(cluster.getServers());
            return new ResponseEntity<>(servers, HttpStatus.OK);
        }
    }

    @PutMapping("cluster/{c_id}/assign_server/{s_id}")
    public ResponseEntity<Cluster> addServer(@PathVariable("c_id") int clusterId, @PathVariable("s_id") int serverId) {
        Cluster cluster = clusterService.getById(clusterId);
        if (cluster == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        Server server = serverService.getById(serverId);
        if (server == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        cluster.getServers().add(server);
        clusterService.update(cluster);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);

    }

    @PutMapping("cluster/{c_id}/remove_server/{s_id}")
    public ResponseEntity<Cluster> removeServer(@PathVariable("c_id") int clusterId, @PathVariable("s_id") int serverId) {
        Cluster cluster = clusterService.getById(clusterId);
        if (cluster == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        Optional<Server> server = cluster.getServers().stream()
                .filter(s -> s.getId() == serverId)
                .findFirst();
        if (!server.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        cluster.getServers().remove(server.get());
        clusterService.update(cluster);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
