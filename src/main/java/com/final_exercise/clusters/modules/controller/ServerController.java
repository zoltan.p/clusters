package com.final_exercise.clusters.modules.controller;

import com.final_exercise.clusters.modules.model.server.Server;
import com.final_exercise.clusters.modules.model.server.ServerState;
import com.final_exercise.clusters.modules.model.server.State;
import com.final_exercise.clusters.modules.persistence.service.server.ServerService;
import com.final_exercise.clusters.modules.persistence.service.server.ServerStateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ServerController implements BaseController<Server> {

    @Autowired
    ServerService serverService;

    @Autowired
    ServerStateService serverStateService;

    @Override
    @PostMapping("/server")
    public ResponseEntity<Server> create(@RequestBody Server server) {
        if (server.getName() == null
                || server.getCpuCount() < 1
                || server.getMemorySizeGb() < 1
                || server.getIpAddress() == null
                || !isValidDotDecimalIp(server.getIpAddress()))
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);

        if (serverService.getByIpAddress(server.getIpAddress()) != null)
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);

        ServerState serverState = createInitialServerState();
        serverStateService.create(serverState);
        server.setActualServerState(serverState);
        return new ResponseEntity<>(serverService.create(server), HttpStatus.CREATED);
    }

    @Override
    @GetMapping("/server/{id}")
    public ResponseEntity<Server> getById(@PathVariable int id) {
        Server server = serverService.getById(id);
        if (server == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(server, HttpStatus.OK);
        }
    }

    @Override
    @GetMapping("/servers")
    public ResponseEntity<List<Server>> listAll() {
        return new ResponseEntity<>(serverService.getAll(), HttpStatus.OK);
    }

    @Override
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleIllegalArgumentException(Exception e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    private ServerState createInitialServerState() {
        ServerState serverState = new ServerState();
        serverState.setState(State.SHUTDOWN);
        return serverState;
    }

    private boolean isValidDotDecimalIp(String ip) {
        return ip.matches("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
    }

}
