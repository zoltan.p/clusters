package com.final_exercise.clusters.modules.controller;

import com.final_exercise.clusters.modules.model.command.Command;
import com.final_exercise.clusters.modules.model.command.CommandType;
import com.final_exercise.clusters.modules.model.server.Server;
import com.final_exercise.clusters.modules.model.server.ServerState;
import com.final_exercise.clusters.modules.model.server.State;
import com.final_exercise.clusters.modules.model.user.User;
import com.final_exercise.clusters.modules.persistence.service.command.CommandService;
import com.final_exercise.clusters.modules.persistence.service.server.ServerService;
import com.final_exercise.clusters.modules.persistence.service.server.ServerStateService;
import com.final_exercise.clusters.modules.persistence.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CommandController {

    @Autowired
    private CommandService commandService;

    @Autowired
    private UserService userService;

    @Autowired
    private ServerService serverService;

    @Autowired
    private ServerStateService serverStateService;

    @PostMapping("/server/{s_id}/user/{u_id}/command")
    public ResponseEntity<Command> create(
            @PathVariable("s_id") int serverId,
            @PathVariable("u_id") int userId,
            @RequestBody Command command) {

        if (command.getType() == null)
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);

        User user = userService.getById(userId);
        Server server = serverService.getById(serverId);

        if (user == null || server == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        boolean userCanUseServer = user.getClusters().stream()
                .anyMatch(c -> c.getServers().contains(server));
        if (!(userCanUseServer && user.isActive()))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        List<CommandType> validCommandTypes =
                getValidCommandTypes(server.getActualServerState().getState());

        if (!validCommandTypes.contains(command.getType()))
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);

        command.setUser(user);

        if (!command.getType().equals(CommandType.RUNPROCESS))
            command.setName(null);

        commandService.create(command);
        ServerState serverState = makeNewFromCommand(command);
        serverStateService.create(serverState);
        server.setActualServerState(serverState);

        serverService.update(server);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @GetMapping("/command_history")
    public ResponseEntity<List<Command>> listAll() {
        return new ResponseEntity<>(commandService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/user/{id}/command_history")
    public ResponseEntity<List<Command>> listAllByUser(@PathVariable int id) {
        return new ResponseEntity<>(commandService.getAllByUserId(id), HttpStatus.OK);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleIllegalArgumentException(Exception e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    private List<CommandType> getValidCommandTypes(State actualState) {
        List<CommandType> validCommandTypes = new ArrayList<>();
        switch (actualState) {
            case SHUTDOWN:
                validCommandTypes.add(CommandType.STARTUP);
                break;
            case WAITING:
                validCommandTypes.add(CommandType.RUNPROCESS);
                validCommandTypes.add(CommandType.SHUTDOWN);
                break;
            case RUNNING:
                validCommandTypes.add(CommandType.STOPPROCESS);
                break;
        }
        return validCommandTypes;
    }

    private ServerState makeNewFromCommand(Command command) {
        ServerState serverState = new ServerState();
        switch (command.getType()) {
            case STARTUP:
                serverState.setState(State.WAITING);
                break;
            case RUNPROCESS:
                serverState.setState(State.RUNNING);
                break;
            case STOPPROCESS:
                serverState.setState(State.WAITING);
                break;
            case SHUTDOWN:
                serverState.setState(State.SHUTDOWN);
        }
        serverState.setProcessName(command.getName());
        return serverState;
    }
}
