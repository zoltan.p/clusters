package com.final_exercise.clusters.modules.model.command;

import com.final_exercise.clusters.modules.model.BaseEntity;
import com.final_exercise.clusters.modules.model.user.User;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Command extends BaseEntity {

    @Column(name = "timestamp", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @ManyToOne
    private User user;
    private CommandType type;

    @Override
    public String toString() {
        return "Command{" +
                "timestamp='" + timestamp + '\'' +
                ", user=" + user.toString() +
                ", type=" + type +
                ", id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CommandType getType() {
        return type;
    }

    public void setType(CommandType type) {
        this.type = type;
    }
}
