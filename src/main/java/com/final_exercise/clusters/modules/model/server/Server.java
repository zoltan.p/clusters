package com.final_exercise.clusters.modules.model.server;


import com.final_exercise.clusters.modules.model.BaseEntity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Server extends BaseEntity {

    @Column(unique = true)
    private String ipAddress;

    @OneToOne(cascade = {CascadeType.ALL})
    private ServerState actualServerState;

    @OneToMany
    @OrderBy("timestamp desc")
    private final Set<ServerState> previousServerStates = new HashSet<>();

    private int cpuCount;
    private int memorySizeGb;

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public ServerState getActualServerState() {
        return actualServerState;
    }

    public void setActualServerState(ServerState serverState) {
        if (this.actualServerState != null)
            previousServerStates.add(this.actualServerState);
        this.actualServerState = serverState;
    }

    public Set<ServerState> getPreviousServerStates() {
        return previousServerStates;
    }

    public int getCpuCount() {
        return cpuCount;
    }

    public void setCpuCount(int cpuCount) {
        this.cpuCount = cpuCount;
    }

    public int getMemorySizeGb() {
        return memorySizeGb;
    }

    public void setMemorySizeGb(int memorySizeGb) {
        this.memorySizeGb = memorySizeGb;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Server server = (Server) o;
        return Objects.equals(id, server.id);
    }

    @Override
    public int hashCode() {
        return id;
    }
}
