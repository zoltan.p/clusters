package com.final_exercise.clusters.modules.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.final_exercise.clusters.modules.model.BaseEntity;
import com.final_exercise.clusters.modules.model.cluster.Cluster;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "user")
public class User extends BaseEntity {

    @ManyToMany(mappedBy = "users")
    @JsonIgnore
    private final
    Set<Cluster> clusters = new HashSet<>();
    private boolean active;

    public User() {
        active = true;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Set<Cluster> getClusters() {
        return clusters;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
