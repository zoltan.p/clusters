package com.final_exercise.clusters.modules.model.cluster;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.final_exercise.clusters.modules.model.BaseEntity;
import com.final_exercise.clusters.modules.model.server.Server;
import com.final_exercise.clusters.modules.model.user.User;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "cluster")
public class Cluster extends BaseEntity {

    @ManyToMany
    @JsonIgnore
    @JoinTable(name = "cluster_users",
            joinColumns = {@JoinColumn(name = "cluster_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")})
    private final
    Set<User> users = new HashSet<>();

    @OneToMany
    private final
    Set<Server> servers = new HashSet<>();

    public Set<User> getUsers() {
        return users;
    }

    public Set<Server> getServers() {
        return servers;
    }
}
