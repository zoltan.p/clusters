package com.final_exercise.clusters.modules.model.command;

public enum CommandType {
    SHUTDOWN, STARTUP, RUNPROCESS, STOPPROCESS
}
