package com.final_exercise.clusters.modules.model.server;

public enum State {
    SHUTDOWN,
    WAITING,
    RUNNING
}
