package com.final_exercise.clusters.modules.persistence.service.cluster;

import com.final_exercise.clusters.modules.model.cluster.Cluster;
import com.final_exercise.clusters.modules.persistence.repository.cluster.ClusterRepository;
import com.final_exercise.clusters.modules.persistence.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.util.Assert.notNull;

@Service
public class ClusterService implements BaseService<Cluster> {

    private final ClusterRepository repository;

    @Autowired
    public ClusterService(ClusterRepository repository) {
        this.repository = repository;
    }

    @Override
    public Cluster create(Cluster cluster) {
        notNull(cluster, "cluster can not be null");
        notNull(cluster.getName(), "clustername can not be null");
        return repository.save(cluster);
    }

    @Override
    public Cluster getById(int id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public void update(Cluster cluster) {
        repository.save(cluster);
    }

    @Override
    public List<Cluster> getAll() {
        List<Cluster> clusters = new ArrayList<>();
        repository.findAll().forEach(clusters::add);
        return clusters;
    }
}
