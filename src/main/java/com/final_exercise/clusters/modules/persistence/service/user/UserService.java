package com.final_exercise.clusters.modules.persistence.service.user;

import com.final_exercise.clusters.modules.model.user.User;
import com.final_exercise.clusters.modules.persistence.repository.user.UserRepository;
import com.final_exercise.clusters.modules.persistence.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService implements BaseService<User> {

    private final UserRepository repository;

    @Autowired
    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public User create(User user) {
        return repository.save(user);
    }

    public User getById(int id) {
        return repository.findById(id).orElse(null);
    }

    public void update(User user) {
        repository.save(user);
    }

    public User getByName(String name) {
        return repository.getByName(name).orElse(null);
    }

    public List<User> getAll() {
        List<User> users = new ArrayList<>();
        repository.findAll().forEach(users::add);
        return users;
    }
}
