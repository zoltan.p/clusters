package com.final_exercise.clusters.modules.persistence.service.server;

import com.final_exercise.clusters.modules.model.server.ServerState;
import com.final_exercise.clusters.modules.persistence.repository.server.ServerStateRepository;
import com.final_exercise.clusters.modules.persistence.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServerStateService implements BaseService<ServerState> {

    private final ServerStateRepository repository;

    @Autowired
    public ServerStateService(ServerStateRepository repository) {
        this.repository = repository;
    }

    @Override
    public ServerState create(ServerState serverState) {
        return repository.save(serverState);
    }

    @Override
    public ServerState getById(int id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public void update(ServerState serverState) {
        repository.save(serverState);
    }

    @Override
    public List<ServerState> getAll() {
        List<ServerState> serverStates = new ArrayList<>();
        repository.findAll().forEach(serverStates::add);
        return serverStates;
    }
}
