package com.final_exercise.clusters.modules.persistence.repository.server;

import com.final_exercise.clusters.modules.model.server.Server;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ServerRepository extends CrudRepository<Server, Integer> {
    Optional<Server> getByIpAddress(String ipAddress);
}
