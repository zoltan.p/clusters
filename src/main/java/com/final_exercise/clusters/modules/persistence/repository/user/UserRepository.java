package com.final_exercise.clusters.modules.persistence.repository.user;

import com.final_exercise.clusters.modules.model.user.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Integer> {
    Optional<User> getByName(String name);
}
