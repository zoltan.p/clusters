package com.final_exercise.clusters.modules.persistence.service.server;

import com.final_exercise.clusters.modules.model.server.Server;
import com.final_exercise.clusters.modules.persistence.repository.server.ServerRepository;
import com.final_exercise.clusters.modules.persistence.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServerService implements BaseService<Server> {

    private final ServerRepository repository;

    @Autowired
    public ServerService(ServerRepository repository) {
        this.repository = repository;
    }

    @Override
    public Server create(Server server) {
        return repository.save(server);
    }

    @Override
    public Server getById(int id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public void update(Server server) {
        repository.save(server);
    }

    @Override
    public List<Server> getAll() {
        List<Server> servers = new ArrayList<>();
        repository.findAll().forEach(servers::add);
        return servers;
    }

    public Server getByIpAddress(String ip) {
        return repository.getByIpAddress(ip).orElse(null);
    }
}
