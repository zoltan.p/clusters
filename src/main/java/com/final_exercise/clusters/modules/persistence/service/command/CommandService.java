package com.final_exercise.clusters.modules.persistence.service.command;

import com.final_exercise.clusters.modules.model.command.Command;
import com.final_exercise.clusters.modules.persistence.repository.command.CommandRepository;
import com.final_exercise.clusters.modules.persistence.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommandService implements BaseService<Command> {

    private final CommandRepository repository;

    @Autowired
    public CommandService(CommandRepository repository) {
        this.repository = repository;
    }

    @Override
    public Command create(Command command) {
        return repository.save(command);
    }

    @Override
    public Command getById(int id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public void update(Command command) {
        repository.save(command);
    }

    @Override
    public List<Command> getAll() {
        List<Command> commands = new ArrayList<>();
        repository.findAllByOrderByTimestampDesc().forEach(commands::add);
        return commands;
    }

    public List<Command> getAllByUserId(int id) {
        List<Command> commands = new ArrayList<>();
        repository.findAllByUserIdOrderByTimestampDesc(id).forEach(commands::add);
        return commands;
    }
}
