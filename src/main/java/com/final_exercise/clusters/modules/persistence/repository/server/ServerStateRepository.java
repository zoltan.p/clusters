package com.final_exercise.clusters.modules.persistence.repository.server;

import com.final_exercise.clusters.modules.model.server.ServerState;
import org.springframework.data.repository.CrudRepository;

public interface ServerStateRepository extends CrudRepository<ServerState, Integer> {
}
