package com.final_exercise.clusters.modules.persistence.service;

import java.util.List;

public interface BaseService<T> {
    T create(T t);

    T getById(int id);

    void update(T t);

    List<T> getAll();
}
