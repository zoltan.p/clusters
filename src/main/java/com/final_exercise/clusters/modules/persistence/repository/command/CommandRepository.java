package com.final_exercise.clusters.modules.persistence.repository.command;

import com.final_exercise.clusters.modules.model.command.Command;
import org.springframework.data.repository.CrudRepository;

public interface CommandRepository extends CrudRepository<Command, Integer> {
    Iterable<Command> findAllByOrderByTimestampDesc();

    Iterable<Command> findAllByUserIdOrderByTimestampDesc(int id);
}
