package com.final_exercise.clusters.modules.persistence.repository.cluster;

import com.final_exercise.clusters.modules.model.cluster.Cluster;
import org.springframework.data.repository.CrudRepository;

public interface ClusterRepository extends CrudRepository<Cluster, Integer> {
}
