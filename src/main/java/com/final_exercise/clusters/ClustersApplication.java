package com.final_exercise.clusters;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.final_exercise.clusters.modules")
public class ClustersApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClustersApplication.class, args);
    }
}
